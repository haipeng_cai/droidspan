# DroidSpan

Studying the deterioration problem of learning-based malware detection and improving the sustainability of malware classifiers.

Java code for Droidspan feature extraction: src/ (for code analysis), and scripts/ (convenience code for different steps such as instrumentation, profiling)

ML code for Droidspan: ML/

Feature extraction and ML code for Afonso as referred to in paper: afonso/

Feature extraction and ML code for DroidSieve as referred to in paper: droidsieve/

Feature extraction and ML code for MamaDroid as referred to in paper: mama/

Feature extraction and ML code for MudFlow as referred to in paper: mudflow/

Feature extraction and ML code for RevealDroid as referred to in paper: revealdroid/


Benchmarks: ML/samplelists
